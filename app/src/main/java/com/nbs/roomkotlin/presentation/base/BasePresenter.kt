package com.nbs.roomkotlin.presentation.base

import android.app.Application
import com.nbs.roomkotlin.data.ContactRepository

open class BasePresenter(application: Application) {
    val repository: ContactRepository = ContactRepository(application)

}