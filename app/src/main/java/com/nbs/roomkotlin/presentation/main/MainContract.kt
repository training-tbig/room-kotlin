package com.nbs.roomkotlin.presentation.main

import com.nbs.roomkotlin.data.ContactItem

interface MainContract{
    interface View{
        fun showContacts(list: MutableList<ContactItem>)
    }

    interface Presenter{
        fun getContacts()
    }
}