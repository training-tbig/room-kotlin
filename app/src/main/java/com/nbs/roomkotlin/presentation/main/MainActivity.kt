package com.nbs.roomkotlin.presentation.main

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.nbs.roomkotlin.R
import com.nbs.roomkotlin.R.layout
import com.nbs.roomkotlin.data.Contact
import com.nbs.roomkotlin.data.ContactItem
import com.nbs.roomkotlin.presentation.addoredit.AddOrEditActivity
import com.nbs.roomkotlin.presentation.main.MainContract.View
import kotlinx.android.synthetic.main.activity_main.rvContacts

class MainActivity : AppCompatActivity(), View, ContactAdapter.OnItemClickListener {

    private lateinit var contactAdapter: ContactAdapter

    private lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)

        rvContacts.setHasFixedSize(true)
        rvContacts.layoutManager = LinearLayoutManager(this)
        rvContacts.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        contactAdapter = ContactAdapter(mutableListOf(), this)
        rvContacts.adapter = contactAdapter

        mainPresenter = MainPresenter(this, this.application)
    }

    override fun onResume() {
        super.onResume()
        mainPresenter.getContacts()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == R.id.action_add){
            AddOrEditActivity.start(this, null)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showContacts(list: MutableList<ContactItem>) {
        if (contactAdapter.list.isNotEmpty()){
            contactAdapter.list.clear()
            contactAdapter.notifyDataSetChanged()
        }

        contactAdapter.list.addAll(list)
        contactAdapter.notifyDataSetChanged()
    }

    override fun onItemClick(contactItem: ContactItem) {
        AddOrEditActivity.start(this, contactItem)
    }
}
