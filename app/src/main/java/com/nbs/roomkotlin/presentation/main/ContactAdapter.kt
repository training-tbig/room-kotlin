package com.nbs.roomkotlin.presentation.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nbs.roomkotlin.R
import com.nbs.roomkotlin.data.Contact
import com.nbs.roomkotlin.data.ContactItem
import com.nbs.roomkotlin.presentation.main.ContactAdapter.Viewholder
import kotlinx.android.synthetic.main.item_contact.view.lnItem
import kotlinx.android.synthetic.main.item_contact.view.tvEmail
import kotlinx.android.synthetic.main.item_contact.view.tvName

class ContactAdapter(val list: MutableList<ContactItem>, private val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<Viewholder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder =
        Viewholder(LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        holder.bind(list[position])
    }

    inner class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(contactItem:  ContactItem){
            itemView.tvName.text = contactItem.name
            itemView.tvEmail.text = contactItem.email
            itemView.lnItem.setOnClickListener {
                onItemClickListener.onItemClick(contactItem)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(contactItem: ContactItem)
    }
}