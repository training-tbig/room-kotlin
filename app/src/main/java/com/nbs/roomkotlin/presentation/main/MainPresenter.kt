package com.nbs.roomkotlin.presentation.main

import android.app.Application
import com.nbs.roomkotlin.data.ContactRepository
import com.nbs.roomkotlin.presentation.base.BasePresenter
import com.nbs.roomkotlin.presentation.main.MainContract.Presenter
import com.nbs.roomkotlin.presentation.main.MainContract.View
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class MainPresenter(private val view: View, application: Application):
    BasePresenter(application), Presenter {

    override fun getContacts() {
        GlobalScope.launch {
            val list = repository.getAllContacts()
            MainScope().launch {
                view.showContacts(list)
            }
        }
    }
}