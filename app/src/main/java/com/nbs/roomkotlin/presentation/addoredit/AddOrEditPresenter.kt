package com.nbs.roomkotlin.presentation.addoredit

import android.app.Application
import com.nbs.roomkotlin.data.Contact
import com.nbs.roomkotlin.presentation.base.BasePresenter
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class AddOrEditPresenter(private val view: AddOrEditContract.View, application: Application):
    BasePresenter(application), AddOrEditContract.Presenter{

    override fun delete(contact: Contact) {
        GlobalScope.launch {
            val isDeleted = repository.delete(contact)
            MainScope().launch {
                if (isDeleted > 0){
                    view.showDeleteMessage("An item deleted")
                }
            }
        }

    }

    override fun save(contact: Contact) {
        GlobalScope.launch {
            val isAdded = repository.insert(contact)
            MainScope().launch {
                if (isAdded){
                    view.showInsertMessage("An item inserted")
                }
            }
        }
    }

    override fun update(contact: Contact) {
        GlobalScope.launch {
            val isUpdated = repository.update(contact)
            MainScope().launch {
                if (isUpdated > 0){
                    view.showUpdateMessage("An item updated")
                }
            }
        }
    }
}