package com.nbs.roomkotlin.presentation.addoredit

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nbs.roomkotlin.R
import com.nbs.roomkotlin.data.Contact
import com.nbs.roomkotlin.data.ContactItem
import com.nbs.roomkotlin.data.mapItemToContact
import kotlinx.android.synthetic.main.activity_add_or_edit.btnDelete
import kotlinx.android.synthetic.main.activity_add_or_edit.btnSave
import kotlinx.android.synthetic.main.activity_add_or_edit.edtEmail
import kotlinx.android.synthetic.main.activity_add_or_edit.edtName

class AddOrEditActivity : AppCompatActivity(), AddOrEditContract.View {

    companion object{
        val EXTRA_CONTACT_ITEM = "EXTRA_CONTACT_ITEM"

        fun start(context: Context, contactItem: ContactItem?){
            val intent = Intent(context, AddOrEditActivity::class.java)
            intent.putExtra(EXTRA_CONTACT_ITEM, contactItem)
            context.startActivity(intent)
        }
    }

    private lateinit var presenter: AddOrEditPresenter

    private var contactItem: ContactItem ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_or_edit)

        contactItem = intent.getParcelableExtra(EXTRA_CONTACT_ITEM)

        supportActionBar?.apply {
            title = if(contactItem == null) "Add New Contact" else "Edit Contact"
            setDisplayHomeAsUpEnabled(true)
        }

        presenter = AddOrEditPresenter(this, this.application)

        if (contactItem == null){
            setAddItem()
        }else{
            setEditItem()
        }

    }

    private fun setEditItem() {
        btnSave.text = "Update"

        edtName.setText(contactItem?.name)
        edtEmail.setText(contactItem?.email)

        btnSave.setOnClickListener {
            val name = edtName.text.toString().trim()
            val email = edtEmail.text.toString().trim()
            var id: Long = 0
            contactItem.let {
                if (it != null) {
                    id = it.id
                }
            }

            if (name.isEmpty() || email.isEmpty()){
                showMessage("Semua field wajib terisi")
            }else{
                presenter.update(Contact(id = id, name = name, email = email))
            }
        }

        btnDelete.setOnClickListener {
            contactItem?.let {
                presenter.delete(mapItemToContact(it))
            }
        }
    }

    private fun setAddItem() {
        btnDelete.visibility = View.GONE
        btnSave.text = "Save"

        btnSave.setOnClickListener {
            val name = edtName.text.toString().trim()
            val email = edtEmail.text.toString().trim()

            if (name.isEmpty() || email.isEmpty()){
                showMessage("Semua field wajib terisi")
            }else{
                presenter.save(Contact(name = name, email = email))
            }
        }
    }

    fun showMessage(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showInsertMessage(message: String) {
        showMessage(message)
        finish()
    }

    override fun showUpdateMessage(message: String) {
        showMessage(message)
        finish()
    }

    override fun showDeleteMessage(message: String) {
        showMessage(message)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }
}
