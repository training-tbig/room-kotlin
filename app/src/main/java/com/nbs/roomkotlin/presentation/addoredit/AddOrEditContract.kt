package com.nbs.roomkotlin.presentation.addoredit

import com.nbs.roomkotlin.data.Contact

interface AddOrEditContract{
    interface View{
        fun showInsertMessage(message: String)

        fun showUpdateMessage(message: String)

        fun showDeleteMessage(message: String)
    }

    interface Presenter{
        fun save(contact: Contact)

        fun update(contact: Contact)

        fun delete(contact: Contact)
    }
}