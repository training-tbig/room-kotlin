package com.nbs.roomkotlin.data

import androidx.room.*

@Dao
interface ContactDao {
    @Query("SELECT * FROM contact_table ORDER by id ASC")
    fun getAllContacts(): MutableList<Contact>

    @Query("SELECT * FROM contact_table where id =:contactId")
    fun getAllContactsById(contactId: Int): MutableList<Contact>

    @Insert
    fun insert(contact: Contact): Long

    @Update
    fun update(contact: Contact): Int

    @Delete
    fun delete(contact: Contact): Int
}