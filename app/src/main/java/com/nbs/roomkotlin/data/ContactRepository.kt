package com.nbs.roomkotlin.data

import android.app.Application
import androidx.annotation.WorkerThread
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class ContactRepository(application: Application) {

    companion object{
        private lateinit var contactDao: ContactDao
    }

    init {
        val db = ContactDb.getDatabase(application)
        if (db != null){
            contactDao = db.contactDao()
        }

    }

    @WorkerThread
    suspend fun getAllContacts(): MutableList<ContactItem> {
        val list = GlobalScope.async {
            contactDao.getAllContacts()
        }

        val mappedList = mutableListOf<ContactItem>()

        list.await().forEach {
            mappedList.add(mapContactToItem(it))
        }

        return mappedList
    }

    @WorkerThread
    suspend fun getAllContactsById(contactId: Int): MutableList<Contact> {
        val list = GlobalScope.async {
            contactDao.getAllContactsById(contactId)
        }
        return list.await()
    }

    @WorkerThread
    suspend fun insert(contact: Contact): Boolean {
        val id = GlobalScope.async {
            contactDao.insert(contact)
        }
        return id.await() > 0
    }

    @WorkerThread
    suspend fun update(contact: Contact): Int {
        val id = GlobalScope.async {
            contactDao.update(contact)
        }
        return id.await()
    }

    @WorkerThread
    suspend fun delete(contact: Contact): Int {
        val id = GlobalScope.async {
            contactDao.delete(contact)
        }
        return id.await()
    }
}