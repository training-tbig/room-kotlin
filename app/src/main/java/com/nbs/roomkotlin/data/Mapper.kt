package com.nbs.roomkotlin.data

fun mapItemToContact(contactItem: ContactItem): Contact{
    return Contact(contactItem.id, contactItem.name, contactItem.email)
}

fun mapContactToItem(contact: Contact): ContactItem{
    return ContactItem(contact.id, contact.name, contact.email)
}