package com.nbs.roomkotlin.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactItem(
    val id: Long,
    val name: String,
    val email: String
) : Parcelable